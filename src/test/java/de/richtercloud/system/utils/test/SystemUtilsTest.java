package de.richtercloud.system.utils.test;

import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;
import org.junit.Test;

public class SystemUtilsTest {

    @Test
    public void testSystemUtils() {
        System.out.println(String.format("java.version: %s",
                System.getProperty("java.version")));
        System.out.println(String.format("Is Java version at least 11: %s",
                SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_11)));
    }
}
